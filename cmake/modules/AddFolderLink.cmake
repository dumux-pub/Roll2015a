# for convienence just uses the dune/dumux infrastructure
macro(add_folder_link folder_name)
  dune_symlink_to_source_files(FILES ${folder_name})
endmacro()
