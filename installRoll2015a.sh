#!/bin/sh

### Create a folder for the dune and dumux modules
### Go into the folder and execute this script

if [ -d dune-common ]; then
  echo "error: A directory named dune-common already exists."
  echo "Aborting."
  exit 1
fi

### Clone the necessary modules
git clone https://gitlab.dune-project.org/core/dune-common.git
git clone https://gitlab.dune-project.org/core/dune-geometry.git
git clone https://gitlab.dune-project.org/core/dune-grid.git
git clone https://gitlab.dune-project.org/core/dune-istl.git
git clone https://gitlab.dune-project.org/core/dune-localfunctions.git

git clone https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git
git clone https://git.iws.uni-stuttgart.de/dumux-pub/Roll2015a.git

### Go to specific branches
cd dune-common && git checkout releases/2.4 && cd ..
cd dune-geometry && git checkout releases/2.4 && cd ..
cd dune-grid && git checkout releases/2.4 && cd ..
cd dune-istl && git checkout releases/2.4 && cd ..
cd dune-localfunctions && git checkout releases/2.4 && cd ..
cd dumux && git checkout releases/2.8 && cd ..


### Go to specific versions
cd dune-common && git checkout fa6bc4af59352430cb05cdb488cbe3ae18dd9547 && cd ..
cd dune-geometry && git checkout ac1fca4ff249ccdc7fb035fa069853d84b93fb73 && cd ..
cd dune-grid && git checkout a2c9bf8c0f0fef025325e059a45b7744225df0d7 && cd ..
cd dune-istl && git checkout 8e13380949417233b1515c8610cb24950382c090 && cd ..
cd dune-localfunctions && git checkout b3a11b4a446ddafc31d51bd6695b8a8a6a1ba30a && cd ..

 ./dune-common/bin/dunecontrol --opts=dumux/optim.opts all