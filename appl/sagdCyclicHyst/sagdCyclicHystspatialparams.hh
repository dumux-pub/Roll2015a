// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Definition of the spatial parameters for the SAGD problem.
 */
#ifndef DUMUX_SAGDCYCLICHYST_SPATIAL_PARAMS_HH
#define DUMUX_SAGDCYCLICHYST_SPATIAL_PARAMS_HH
#include <dumux/implicit/3p2cni/3p2cniindices.hh>
#include <dumux/material/spatialparams/implicitspatialparams.hh>
//#include <dumux/material/fluidmatrixinteractions/3p/relativPermHyst3p.hh>
//#include <dumux/material/fluidmatrixinteractions/3p/relativPermHyst3pparams.hh>

#include <dumux/material/fluidmatrixinteractions/3p/parkerVanGenZeroHyst3p.hh>
#include <dumux/material/fluidmatrixinteractions/3p/parkerVanGenZeroHyst3pparams.hh>

namespace Dumux
{
//forward declaration
template<class TypeTag>
class SagdCyclicHystSpatialParams;
namespace Properties
{
// The spatial parameters TypeTag
NEW_TYPE_TAG(SagdCyclicHystSpatialParams);
// Set the spatial parameters
SET_TYPE_PROP(SagdCyclicHystSpatialParams, SpatialParams, Dumux::SagdCyclicHystSpatialParams<TypeTag>);
// Set the material Law
SET_PROP(SagdCyclicHystSpatialParams, MaterialLaw)
{
 private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
 public:
    typedef ParkerVanGenZeroHyst3P<Scalar> type;
};
}
/*!
 * \ingroup ThreePThreeCNIModel
 *
 * \brief Definition of the spatial parameters for the SAGD problem
 */
template<class TypeTag>
class SagdCyclicHystSpatialParams : public ImplicitSpatialParams<TypeTag>
{
    typedef ImplicitSpatialParams<TypeTag> ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
    typedef typename GET_PROP_TYPE(TypeTag, Grid) Grid;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename Grid::ctype CoordScalar;

    enum {
        dim=GridView::dimension,
        dimWorld=GridView::dimensionworld
    };

    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
    enum {

        numPhases = GET_PROP_VALUE(TypeTag, NumPhases),
        numComponents = GET_PROP_VALUE(TypeTag, NumComponents),

    wPhaseIdx = Indices::wPhaseIdx,
        nPhaseIdx = Indices::nPhaseIdx,
    gPhaseIdx = Indices::gPhaseIdx
    };

    typedef Dune::FieldVector<CoordScalar,dimWorld> GlobalPosition;
    typedef Dune::FieldVector<CoordScalar,dimWorld> DimVector;


    typedef typename GET_PROP_TYPE(TypeTag, SolutionVector) SolutionVector;

    typedef typename GET_PROP_TYPE(TypeTag, FluxVariables) FluxVariables;
    typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;

    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename GridView::template Codim<0>::Entity Element;

    typedef typename GridView::template Codim<0>::Iterator ElementIterator;
    typedef typename GridView::template Codim<dim>::Iterator VertexIterator;
    typedef typename GET_PROP_TYPE(TypeTag, VolumeVariables) VolumeVariables;

    enum { isBox = GET_PROP_VALUE(TypeTag, ImplicitIsBox) };
    enum { dofCodim = isBox ? dim : 0 };


public:
    typedef typename GET_PROP_TYPE(TypeTag, MaterialLaw) MaterialLaw;
    typedef typename MaterialLaw::Params MaterialLawParams;
   typedef std::vector<MaterialLawParams> MaterialLawHystParamsVector;

    /*!
     * \brief The constructor
     *
     * \param gridView The grid view
     */
    SagdCyclicHystSpatialParams(const GridView &gridView)
        : ParentType(gridView)
    {
    layerBottom_ = 85.0;
        // intrinsic permeabilities
        fineK_ = 1e-16;
        coarseK_ = 4e-14;
        // porosities
        finePorosity_ = 0.10;
        coarsePorosity_ = 0.1;
        // heat conductivity of granite
        //lambdaSolid_ = 2.8;
        // specific heat capacities
        fineHeatCap_ = 850.;
        coarseHeatCap_ = 850;
        // residual saturations
             fineMaterialParams_.setSwr(0.1);
                fineMaterialParams_.setSwrx(0.12);  //Total liquid Residual Saturation
                fineMaterialParams_.setSnr(0.09);   //Residual of NAPL if there is no water
                fineMaterialParams_.setSgr(0.01);
                coarseMaterialParams_.setSwr(0.1);
                coarseMaterialParams_.setSwrx(0.12);
                coarseMaterialParams_.setSnr(0.09);
                coarseMaterialParams_.setSgr(0.01);

    // parameters for the 3phase van Genuchten law
        fineMaterialParams_.setVgN(4.0);
        coarseMaterialParams_.setVgN(4.0);
        fineMaterialParams_.setVgAlpha(0.0005);
        coarseMaterialParams_.setVgAlpha(0.0015);
        coarseMaterialParams_.setkrRegardsSnr(false);
        fineMaterialParams_.setkrRegardsSnr(false);

    }
    ~SagdCyclicHystSpatialParams()
    {}
    /*!

     * \brief Update the spatial parameters with the flow solution
     *        after a timestep.
     *
     * \param globalSolution The global solution vector
     */
    void update(const SolutionVector &globalSolution)
    {
    };

    /*!
     * \brief Apply the intrinsic permeability tensor to a pressure
     *        potential gradient.
     *
     * \param element The current finite element
     * \param fvElemGeom The current finite volume geometry of the element
     * \param scvIdx The index of the sub-control volume
     */
    const Scalar intrinsicPermeability(const Element &element,
                                       const FVElementGeometry &fvElemGeom,
                                       int scvIdx) const
    {
        const GlobalPosition &pos = fvElemGeom.subContVol[scvIdx].global;
        if (isFineMaterial_(pos))
            return fineK_;
        return coarseK_;
    }

    /*!
     * \brief Define the porosity \f$[-]\f$ of the spatial parameters
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry
     * \param scvIdx The local index of the sub-control volume where
     *                    the porosity needs to be defined
     */
    double porosity(const Element &element,
                    const FVElementGeometry &fvGeometry,
                    const int scvIdx) const
    {
        const GlobalPosition &pos = fvGeometry.subContVol[scvIdx].global;
        if (isFineMaterial_(pos))
            return finePorosity_;
        else
            return coarsePorosity_;
    }


    /*!
     * \brief return the parameter object for the Brooks-Corey material law which depends on the position
     *
     * \param element The current finite element
     * \param fvGeometry The current finite volume geometry of the element
     * \param scvIdx The index of the sub-control volume
     */
    const MaterialLawParams& materialLawParams(const Element &element,
                                               const FVElementGeometry &fvGeometry,
                                               const int scvIdx) const
    {
        const GlobalPosition &pos = fvGeometry.subContVol[scvIdx].global;
        if (isFineMaterial_(pos))
            return fineMaterialParams_;
        else
            return coarseMaterialParams_;


    }

    /*!
     * \brief Returns the heat capacity \f$[J/m^3 K]\f$ of the rock matrix.
     *
     * This is only required for non-isothermal models.
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry
     * \param scvIdx The local index of the sub-control volume where
     *                    the heat capacity needs to be defined
     */
    double heatCapacity(const Element &element,
                        const FVElementGeometry &fvGeometry,
                        const int scvIdx) const
    {
        const GlobalPosition &pos = fvGeometry.subContVol[scvIdx].global;
        if (isFineMaterial_(pos))
            return fineHeatCap_ * 2650 // density of sand [kg/m^3]
                * (1 - porosity(element, fvGeometry, scvIdx));
        else
            return coarseHeatCap_ * 2650 // density of sand [kg/m^3]
                * (1 - porosity(element, fvGeometry, scvIdx));
    }

    /*!
     * \brief Calculate the heat flux \f$[W/m^2]\f$ through the
     *        rock matrix based on the temperature gradient \f$[K / m]\f$
     *
     * This is only required for non-isothermal models.
     *
     * \param heatFlux The resulting heat flux vector
     * \param fluxDat The flux variables
     * \param elemVolVars The volume variables
     * \param tempGrad The temperature gradient
     * \param element The current finite element
     * \param fvGeometry The finite volume geometry of the current element
     * \param faceIdx The local index of the sub-control volume face where
     *                    the matrix heat flux should be calculated
     */
    void matrixHeatFlux(DimVector &heatFlux,
                        const FluxVariables &fluxDat,
                        const ElementVolumeVariables &elemVolVars,
                        const DimVector &tempGrad,
                        const Element &element,
                        const FVElementGeometry &fvGeometry,
                        int faceIdx) const
    {
        static const Scalar ldry = 0.35;
        static const Scalar lSw1 = 1.8;
        static const Scalar lSn1 = 0.65;

        // arithmetic mean of the liquid saturation and the porosity
        const int i = fvGeometry.subContVolFace[faceIdx].i;
        const int j = fvGeometry.subContVolFace[faceIdx].j;
        Scalar Sw = std::max(0.0, (elemVolVars[i].saturation(wPhaseIdx) +
                                   elemVolVars[j].saturation(wPhaseIdx)) / 2);
        Scalar Sn = std::max(0.0, (elemVolVars[i].saturation(nPhaseIdx) +
                                   elemVolVars[j].saturation(nPhaseIdx)) / 2);

        // the heat conductivity of the matrix. in general this is a
        // tensorial value, but we assume isotropic heat conductivity.
        Scalar heatCond = ldry + sqrt(Sw) * (lSw1-ldry) + sqrt(Sn) * (lSn1-ldry);

        // the matrix heat flux is the negative temperature gradient
        // times the heat conductivity.
        heatFlux = tempGrad;
        heatFlux *= -heatCond;
    }
struct MaxSaturations
    {
    double MaxSatW;
    double MaxSatN;
    double MaxSatG;

    };

struct trappedSaturations
    {
    double trappedSatN;

    };

 void getMaxSaturation(Problem &problem)
        {

    typedef Dune::BlockVector<Dune::FieldVector<double, 1> > ScalarField;

         // get the number of degrees of freedom
         unsigned numDofs = problem.model().numDofs();
         unsigned numElements = problem.gridView().size(0);
     const ElementVolumeVariables elemVolVars;
         ElementIterator eIt = problem.gridView().template begin<0>();
         ElementIterator eEndIt = problem.gridView().template end<0>();
         FVElementGeometry fvGeometry;
         VolumeVariables volVars;
         maxSats_.resize(numDofs);

     ScalarField *getMaxSaturation[numPhases];

         for (; eIt != eEndIt; ++eIt)
         {
             int eIdx = problem.elementMapper().map(*eIt);
           //  (*rank)[eIdx] = problem.gridView().comm().rank();
             fvGeometry.update(problem.gridView(), *eIt);

             for (int scvIdx = 0; scvIdx < fvGeometry.numScv; ++scvIdx)
             {
                 int globalIdx = problem.model().dofMapper().map(*eIt, scvIdx, dofCodim);
                 volVars.update(problem.model().curSol()[globalIdx],
                                problem,
                                *eIt,
                                fvGeometry,
                                scvIdx,
                                false);
      //  MaterialLaw::update
        // std::cout << "the global index" << globalIdx << std::endl;
        //std::cout << "the volVars entry of saturation wPhaseIdx " << volVars.saturation(wPhaseIdx) << std::endl;
         //std::cout << "the maxSats entry of saturation MaxSatW " << maxSats_[globalIdx].MaxSatW << std::endl;
    //std::cout << "the volVars entry of saturation nPhaseIdx " << volVars.saturation(nPhaseIdx) << std::endl;
       //  std::cout << "the maxSats entry of saturation MaxSatN " << maxSats_[globalIdx].MaxSatN << std::endl;

     if (volVars.saturation(wPhaseIdx)>maxSats_[globalIdx].MaxSatW)
         {
    maxSats_[globalIdx].MaxSatW = volVars.saturation(wPhaseIdx);

     };

     if (volVars.saturation(wPhaseIdx)>maxSats_[globalIdx].MaxSatW)
         {
    maxSats_[globalIdx].MaxSatW = volVars.saturation(wPhaseIdx);
     }


     if (volVars.saturation(nPhaseIdx)>maxSats_[globalIdx].MaxSatN)
         {
            maxSats_[globalIdx].MaxSatN = volVars.saturation(nPhaseIdx);

     }
     if (volVars.saturation(gPhaseIdx)>maxSats_[globalIdx].MaxSatG)
         {
            maxSats_[globalIdx].MaxSatG = volVars.saturation(gPhaseIdx);
     }

     }

     }

}

void trappedSat(Problem &problem)
        {

    typedef Dune::BlockVector<Dune::FieldVector<double, 1> > ScalarField;

         // get the number of degrees of freedom
         unsigned numDofs = problem.model().numDofs();
         unsigned numElements = problem.gridView().size(0);
     const ElementVolumeVariables elemVolVars;
         ElementIterator eIt = problem.gridView().template begin<0>();
         ElementIterator eEndIt = problem.gridView().template end<0>();
         FVElementGeometry fvGeometry;
         VolumeVariables volVars;
         trapSats_.resize(numDofs);

    // ScalarField *trappedSat[numPhases];
         for (; eIt != eEndIt; ++eIt)
         {
             int eIdx = problem.elementMapper().map(*eIt);
             fvGeometry.update(problem.gridView(), *eIt);
             for (int scvIdx = 0; scvIdx < fvGeometry.numScv; ++scvIdx)
             {
                 int globalIdx = problem.model().dofMapper().map(*eIt, scvIdx, dofCodim);
                 volVars.update(problem.model().curSol()[globalIdx],
                                problem,
                                *eIt,
                                fvGeometry,
                                scvIdx,
                                false);
trapSats_[globalIdx].trappedSatN =
(maxSats_[globalIdx].MaxSatN)/(1+9*maxSats_[globalIdx].MaxSatN);

            coarseMaterialParams_.setTrappedSatN(trapSats_[globalIdx].trappedSatN);
            fineMaterialParams_.setTrappedSatN(trapSats_[globalIdx].trappedSatN);
     }
     }
}

  /*  void addOutputVtkFields(Problem &problem)
                {
                    typedef Dune::BlockVector<Dune::FieldVector<double, 1> > ScalarField;
                    unsigned numVertices = problem.gridView().size(dim);

                    //create required scalar fields
                    ScalarField *trappedSaturationN = problem.resultWriter().allocateManagedBuffer(numVertices);

                    //Fill the scalar fields with values
                    unsigned numElements = problem.gridView().size(0);
                    ScalarField *rank = problem.resultWriter().allocateManagedBuffer(numElements);

                    FVElementGeometry fvElemGeom;
                    VolumeVariables volVars;

                    ElementIterator elemIt = problem.gridView().template begin<0>();
                    ElementIterator elemEndIt = problem.gridView().template end<0>();
                    for (; elemIt != elemEndIt; ++elemIt)
                    {
                        int idx = problem.elementMapper().map(*elemIt);
                        (*rank)[idx] = problem.gridView().comm().rank();
                        fvElemGeom.update(problem.gridView(), *elemIt);

                        int numVerts = elemIt->template count<dim> ();

                        for (int i = 0; i < numVerts; ++i)
                        {
                            int globalIdx = problem.vertexMapper().map(*elemIt, i, dim);


                            (*trappedSaturationN)[globalIdx] = trapSats_[globalIdx].trappedSatN;
            }
                    }

                    //pass the scalar fields to the vtkwriter
                   // this->resultWriter().attachVertexData(*boxVolume, "boxVolume");
                    problem.resultWriter().attachVertexData(*trappedSaturationN, "trappedSaturationN");

                }*/
    std::vector<trappedSaturations> trapSats_;
private:
    bool isFineMaterial_(const GlobalPosition &pos) const
    {
        /*if (0.90 <= pos[1])
            return true;
        else return false;*/
        return pos[dim-1] > layerBottom_;
    };

    Scalar layerBottom_;
    Scalar lambdaSolid_;

    Scalar fineK_;
    Scalar coarseK_;

    Scalar finePorosity_;
    Scalar coarsePorosity_;

    Scalar fineHeatCap_;
    Scalar coarseHeatCap_;

    MaterialLawParams fineMaterialParams_;
    MaterialLawParams coarseMaterialParams_;
    std::vector<MaxSaturations> maxSats_;

    //std::vector<MaterialLawParams> maxSats_;
    MaterialLawHystParamsVector materialLawHystParams_;


};

}

#endif
