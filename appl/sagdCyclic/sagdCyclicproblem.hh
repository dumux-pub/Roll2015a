// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later vesion.                                      *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Non-isothermal SAGD problem 
 *      
 */

#ifndef DUMUX_SAGDCYCLICPROBLEM_HH
#define DUMUX_SAGDCYCLICPROBLEM_HH

#include <dune/grid/io/file/dgfparser/dgfug.hh>
#include <dune/grid/io/file/dgfparser/dgfs.hh>
#include <dune/grid/io/file/dgfparser/dgfyasp.hh>

#include <dumux/implicit/3p2cni/3p2cnimodel.hh>
#include <dumux/implicit/common/implicitporousmediaproblem.hh>

#include "sagdCyclicspatialparams.hh"
#include <dumux/material/fluidsystems/h2oheavyoilfluidsystem.hh>

#define ISOTHERMAL 0
double massProducedOil_;
double massProducedWater_;
namespace Dumux
{
template <class TypeTag>
class SagdCyclicProblem;

namespace Properties
{
NEW_TYPE_TAG(SagdCyclicProblem, INHERITS_FROM(ThreePTwoCNI, SagdCyclicSpatialParams));
NEW_TYPE_TAG(SagdCyclicBoxProblem, INHERITS_FROM(BoxModel, SagdCyclicProblem));
NEW_TYPE_TAG(SagdCyclicCCProblem, INHERITS_FROM(CCModel, SagdCyclicProblem));
    
// Set the grid type
SET_PROP(SagdCyclicProblem, Grid)
{
    typedef Dune::YaspGrid<2> type;
};

// Set the problem property
SET_PROP(SagdCyclicProblem, Problem)
{
    typedef Dumux::SagdCyclicProblem<TypeTag> type;
};

// Set the fluid system
SET_TYPE_PROP(SagdCyclicProblem,
              FluidSystem,
              Dumux::FluidSystems::H2OHeavyOil<TypeTag, typename GET_PROP_TYPE(TypeTag, Scalar)>);


// Enable gravity
SET_BOOL_PROP(SagdCyclicProblem, ProblemEnableGravity, true);

// Use forward differences instead of central differences
SET_INT_PROP(SagdCyclicProblem, ImplicitNumericDifferenceMethod, +1);

// Write newton convergence
SET_BOOL_PROP(SagdCyclicProblem, NewtonWriteConvergence, false);

SET_BOOL_PROP(SagdCyclicProblem, UseSimpleModel, true);

// Set the maximum time step
//SET_SCALAR_PROP(SagdCyclicProblem, TimeManagerMaxTimeStepvectorSize, 4.);
}

/*!
 * \ingroup ThreePTwoCNIBoxModel
 * \ingroup ImplicitTestProblems
 * \brief Non-isothermal problem where ...
 *
 * This problem uses the \ref ThreePTwoCNIModel.
 *
 *  */
template <class TypeTag >
class SagdCyclicProblem : public ImplicitPorousMediaProblem<TypeTag>
{
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GridView::Grid Grid;

    typedef ImplicitPorousMediaProblem<TypeTag> ParentType;

    // copy some indices for convenience
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
    enum {
        pressureIdx = Indices::pressureIdx,
        switch1Idx = Indices::switch1Idx,
        switch2Idx = Indices::switch2Idx,

        energyEqIdx = Indices::energyEqIdx,

        // phase and component indices
        wPhaseIdx = Indices::wPhaseIdx,
        nPhaseIdx = Indices::nPhaseIdx,
        gPhaseIdx = Indices::gPhaseIdx,
        wCompIdx = Indices::wCompIdx,
        nCompIdx = Indices::nCompIdx,

        // Phase State
        wPhaseOnly = Indices::wPhaseOnly,
        wnPhaseOnly = Indices::wnPhaseOnly,
        wgPhaseOnly = Indices::wgPhaseOnly,
        threePhases = Indices::threePhases,

        // Grid and world dimension
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld
    };


    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, BoundaryTypes) BoundaryTypes;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GridView::template Codim<dim>::Entity Vertex;
    typedef typename GridView::Intersection Intersection;

    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
    typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;

    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;

    enum { isBox = GET_PROP_VALUE(TypeTag, ImplicitIsBox) };
    
public:
    /*!
     * \brief The constructor
     *
     * \param timeManager The time manager
     * \param gridView The grid view
     */
    SagdCyclicProblem(TimeManager &timeManager, const GridView &gridView)
        : ParentType(timeManager, gridView), eps_(1e-6), pOut_(4.0e6)
    {
        maxDepth_ = 440.0; // [m]
        FluidSystem::init();
    totalMassProducedOil_ =0;
    totalMassProducedWater_ =0;

        this->timeManager().startNextEpisode(3600*12.);
        name_ = GET_RUNTIME_PARAM(TypeTag, std::string, Problem.Name);
    }

      bool shouldWriteRestartFile() const
      {
          return
            this->timeManager().timeStepIndex() == 0 ||
            this->timeManager().timeStepIndex() % 100 == 0 ||
            this->timeManager().episodeWillBeOver() ||
            this->timeManager().willBeFinished();
      }

      bool shouldWriteOutput() const
      {
          return
            this->timeManager().timeStepIndex() == 0 ||
            this->timeManager().timeStepIndex() % 100 == 0 ||
            this->timeManager().episodeWillBeOver() ||
            this->timeManager().willBeFinished();
      }


    void postTimeStep()
    {
        double time = this->timeManager().time();
        double dt = this->timeManager().timeStepSize();

        // Calculate storage terms
        PrimaryVariables storage;
        this->model().globalStorage(storage);

        // Write mass balance information for rank 0
        if (this->gridView().comm().rank() == 0)
        {
            std::cout<<"Storage: " << storage << " Time: " << time+dt << std::endl;
            massBalance.open ("massBalanceCyclic.txt", std::ios::out | std::ios::app );
                        massBalance << "         Storage       " << storage
                                    << "           Time           " << time+dt
                                    << std::endl; "\n";
                        massBalance.close();

        }

    //mass of Oil
    Scalar newMassProducedOil_ = massProducedOil_;
    //std::cout<<" newMassProducedOil_ : "<< newMassProducedOil_ << " Time: " << time+dt << std::endl;

    totalMassProducedOil_ += newMassProducedOil_;
    //std::cout<<" totalMassProducedOil_ : "<< totalMassProducedOil_ << " Time: " << time+dt << std::endl; 
    //mass of Water
    Scalar newMassProducedWater_ = massProducedWater_;
    //std::cout<<" newMassProducedWater_ : "<< newMassProducedWater_ << " Time: " << time+dt << std::endl;

    totalMassProducedWater_ += newMassProducedWater_;
    //std::cout<<" totalMassProducedWater_ : "<< totalMassProducedWater_ << " Time: " << time+dt << std::endl;

    int timeStepIndex = this->timeManager().timeStepIndex();

    if (    this->timeManager().timeStepIndex() == 0 ||
        this->timeManager().timeStepIndex() % 100 == 0 ||   
                this->timeManager().episodeWillBeOver() ||
                this->timeManager().willBeFinished()
    )

    {
    //std::cout<<" newMassProducedOil_ : "<< newMassProducedOil_ << " Time: " << time+dt << std::endl;
    std::cout<<" totalMassProducedOil_ : "<< totalMassProducedOil_ << " Time: " << time+dt << std::endl; 
    //std::cout<<" newMassProducedWater_ : "<< newMassProducedWater_ << " Time: " << time+dt << std::endl;
    std::cout<<" totalMassProducedWater_ : "<< totalMassProducedWater_ << " Time: " << time+dt << std::endl;
    }

    //this->spatialParams().getMaxSaturation(*this);
    //this->spatialParams().trappedSat(*this);
    }

    void episodeEnd()
    {
        indexEpisode = this->timeManager().episodeIndex();

        // Start new episode if episode is over
        this->timeManager().startNextEpisode(3600*12);   //episode length sent to 12 hours
            std::cout<<"Episode index is set to: "<<indexEpisode <<std::endl;
            
//            if (indexEpisode % 2 == 0){
            if (std::fmod(indexEpisode,2) == 0){
            std::cout<<"Episode index is : "<<indexEpisode << ". Which is an even Episode Index and this is a Non-Injection Phase "<<std::endl;}
        else {
             std::cout<<"Episode index is : "<<indexEpisode << ". Which is an * Odd * Episode Index and this is a *Injection Phase* "<<std::endl;
            }
    }

    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const std::string name() const
    { return name_; }


    void sourceAtPos(PrimaryVariables &values,
                     const GlobalPosition &globalPos) const
    {
          values = 0.0;
    }

    // \}

    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment.
     *
     * \param values The boundary types for the conservation equations
     * \param globalPos The position for which the bc type should be evaluated
     */
   void boundaryTypesAtPos(BoundaryTypes &bcTypes,
            const GlobalPosition &globalPos) const
    {
        if (globalPos[1] <  eps_)                               // on bottom
        {
                bcTypes.setAllNeumann();
        }
        else if (globalPos[1] >  90.-eps_)                               // on top
        {
                bcTypes.setAllNeumann();                      
        }

        else if ( globalPos[0] > (60 - eps_) )
        {
            bcTypes.setAllDirichlet();
        }


        else                                                    // on Left 
             {
                bcTypes.setAllNeumann();
             }
    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        boundary segment.
     *
     * \param values The dirichlet values for the primary variables
     * \param globalPos The position for which the bc type should be evaluated
     *
     * For this method, the \a values parameter stores primary variables.
     */
    void dirichletAtPos(PrimaryVariables &values, const GlobalPosition &globalPos) const
    {

       initial_(values, globalPos);        // Everywhere else        

    }

    /*!
     * \brief Evaluate the boundary conditions for a neumann
     *        boundary segment.
     *
     * \param values The neumann values for the conservation equations
     * \param element The finite element
     * \param fvGeomtry The finite-volume geometry in the box scheme
     * \param is The intersection between element and boundary
     * \param scvIdx The local vertex index
     * \param boundaryFaceIdx The index of the boundary face
     *
     * For this method, the \a values parameter stores the mass flux
     * in normal direction of each phase. Negative values mean influx.
     */
    void solDependentNeumann(PrimaryVariables &values,
                      const Element &element,
                      const FVElementGeometry &fvGeometry,
                      const Intersection &is,
                      const int scvIdx,
                      const int boundaryFaceIdx,
                      const ElementVolumeVariables &elemVolVars) const
    {
        values = 0.0;
        int indexEpisode;
        indexEpisode = this->timeManager().episodeIndex();

        GlobalPosition globalPos;
        if (isBox)
           globalPos = element.geometry().corner(scvIdx);
        else
           globalPos = is.geometry().center();


        if (globalPos[1] > 48.5  &&                     // negative values for injection at injection well
            globalPos[1] < 49.5  )
        {
            if (indexEpisode % 2 == 0)          // if episodeIndex is even >> noInjection Phase
                 {
            values[Indices::contiNEqIdx] = 0.0;
            values[Indices::contiWEqIdx] = 0.0;
            values[Indices::energyEqIdx] = 0.0;
                  }
            else
                  {                               // if episodeIndex is odd >> Injection Phase
            values[Indices::contiNEqIdx] = -0.0;  
            values[Indices::contiWEqIdx] = -0.193*2;   // (55.5 mol*47.6)/3600 mol/s m = 0.733833333333
            values[Indices::energyEqIdx] = -9132*2;   // J/sec m 34788.3611111
                  }
        }
        

        else if (globalPos[1] > 42.5  && globalPos[1] < 43.5) // production well
        {

            Scalar satW = elemVolVars[scvIdx].saturation(wPhaseIdx);                // Saturations
            Scalar satG = elemVolVars[scvIdx].saturation(gPhaseIdx);
            Scalar satN = elemVolVars[scvIdx].saturation(nPhaseIdx);
    
            Scalar elemPressW = elemVolVars[scvIdx].pressure(wPhaseIdx);            //Pressures
            Scalar elemPressG = elemVolVars[scvIdx].pressure(gPhaseIdx);
            Scalar elemPressN = elemVolVars[scvIdx].pressure(nPhaseIdx);
    
            Scalar densityW = elemVolVars[scvIdx].fluidState().density(wPhaseIdx);  //Densities
            Scalar densityG = elemVolVars[scvIdx].fluidState().density(gPhaseIdx);
            Scalar densityN = elemVolVars[scvIdx].fluidState().density(nPhaseIdx);
    
            Scalar moDensityW = elemVolVars[scvIdx].fluidState().molarDensity(wPhaseIdx);   //Molar Densities
            Scalar moDensityG = elemVolVars[scvIdx].fluidState().molarDensity(gPhaseIdx);
            Scalar moDensityN = elemVolVars[scvIdx].fluidState().molarDensity(nPhaseIdx);
    
            Scalar maDensityW = elemVolVars[scvIdx].fluidState().density(wPhaseIdx);   //Molar Densities
            Scalar maDensityG = elemVolVars[scvIdx].fluidState().density(gPhaseIdx);
            Scalar maDensityN = elemVolVars[scvIdx].fluidState().density(nPhaseIdx);
            Scalar elemMobW = elemVolVars[scvIdx].mobility(wPhaseIdx);      //Mobilities
            Scalar elemMobG = elemVolVars[scvIdx].mobility(gPhaseIdx);
            Scalar elemMobN = elemVolVars[scvIdx].mobility(nPhaseIdx);

            Scalar molFracWinW = elemVolVars[scvIdx].fluidState().moleFraction(wPhaseIdx, wCompIdx);
            Scalar molFracWinN = elemVolVars[scvIdx].fluidState().moleFraction(nPhaseIdx, wCompIdx);
            Scalar molFracWinG = elemVolVars[scvIdx].fluidState().moleFraction(gPhaseIdx, wCompIdx);
            Scalar molFracNinW = elemVolVars[scvIdx].fluidState().moleFraction(wPhaseIdx, nCompIdx);
            Scalar molFracNinN = elemVolVars[scvIdx].fluidState().moleFraction(nPhaseIdx, nCompIdx);
            Scalar molFracNinG = elemVolVars[scvIdx].fluidState().moleFraction(gPhaseIdx, nCompIdx);

            Scalar enthW = elemVolVars[scvIdx].enthalpy(wPhaseIdx);      //Mobilities
            Scalar enthG = elemVolVars[scvIdx].enthalpy(gPhaseIdx);
            Scalar enthN = elemVolVars[scvIdx].enthalpy(nPhaseIdx);

            Scalar wellRadius = 0.50 * 0.3048; // 0.50 ft as specified by SPE9

            Scalar gridHeight_ = 0.5;
            Scalar effectiveRadius_ = 0.4;
         
            Scalar qW = (((2*3.1415*0.5*4e-14)/(std::log(effectiveRadius_/wellRadius))) *   //divided by molarMass() of water to convert from kg/m s to mol/m s 
            densityW * elemMobW * ( elemPressW-pOut_));
    
            Scalar qN = (((2*3.1415*0.5*4e-14)/(std::log(effectiveRadius_/wellRadius))) * 
            densityN * elemMobN  * (elemPressN-pOut_)); //divided by molarMass() of HeavyOil to convert from kg/m s to mol/m s
    
            //Without cooling:
            //Scalar qE = qW*0.018*enthW + qN*enthN*0.350;
        
            Scalar wT = elemVolVars[scvIdx].temperature(); // well temperature
            Scalar qE;
            if ( wT > 495. )
               {
                    qE = qW*0.018*enthW + qN*enthN*0.350 + (wT-495.)*5000.; // ~3x injected enthalpy
                    std::cout<< "Cooling now! Extracted enthalpy: " << qE << std::endl;
               }                        
            else
               {
                    qE = qW*0.018*enthW + qN*enthN*0.350;
               }

    values[Indices::contiWEqIdx] = qW;

        values[Indices::contiNEqIdx] = qN;
  
        values[Indices::energyEqIdx] = qE;

    massProducedOil_ = qN;
    massProducedWater_ = qW;

        }

}

    // \}

    /*!
     * \name Volume terms
     */
    // \{

    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * \param values The initial values for the primary variables
     * \param globalPos The position for which the initial condition should be evaluated
     *
     * For this method, the \a values parameter stores primary
     * variables.
     */
    void initialAtPos(PrimaryVariables &values, const GlobalPosition &globalPos) const
    {
        initial_(values, globalPos);
    }

    /*!
     * \brief Return the initial phase state inside a control volume.
     *
     * \param vert The vertex
     * \param globalIdx The index of the global vertex
     * \param globalPos The global position
     */
    int initialPhasePresence(const Vertex &vert,
                             int &globalIdx,
                             const GlobalPosition &globalPos) const
    {
        return wnPhaseOnly;
        //return threePhases;
    }

private:
    // internal method for the initial condition (reused for the
    // dirichlet conditions!)
    void initial_(PrimaryVariables &values,
                  const GlobalPosition &globalPos) const
    {
        Scalar densityW = 1000.0;
        values[pressureIdx] = 101300. + (maxDepth_ - globalPos[1])*densityW*9.81;

        values[switch1Idx] = 295.13;   // temperature
        values[switch2Idx] = 0.3;   //NAPL saturation
    }

    Scalar maxDepth_;
    Scalar eps_;
    Scalar pIn_;
    Scalar pOut_;
    Scalar totalMassProducedOil_;
    Scalar totalMassProducedWater_;
    //int vectorSize;
    Scalar episodeLength_;
    Scalar indexEpisode;
    std::string name_;
   // std::string name_;
    //std::ifstream inputFile; //Input file stream object
    //std::vector<double> hours;
    std::ofstream massBalance;


};
} //end namespace

#endif
