// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later vesion.                                      *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Non-isothermal SAGD problem 
 *      
 */
#ifndef DUMUX_SAGDLABCYCLICHYSTPROBLEM_HH
#define DUMUX_SAGDLABCYCLICHYSTPROBLEM_HH

#include <dune/grid/io/file/dgfparser/dgfug.hh>
#include <dune/grid/io/file/dgfparser/dgfs.hh>
#include <dune/grid/io/file/dgfparser/dgfyasp.hh>

#include <dumux/implicit/3p2cni/3p2cnimodel.hh>
//#include <dumux/implicit/3p2cnihysteresis/3p2cnihysteresismodel.hh>
#include <dumux/implicit/common/implicitporousmediaproblem.hh>

#include "sagdLabCyclicHystspatialparams.hh"
#include <dumux/material/fluidsystems/h2omediumoilfluidsystem.hh>
#include <algorithm>
#define ISOTHERMAL 0
double massProducedOil_;
double massProducedWater_;

namespace Dumux
{
template <class TypeTag>
class SagdLabCyclicHystProblem;

namespace Properties
{
NEW_TYPE_TAG(SagdLabCyclicHystProblem, INHERITS_FROM(ThreePTwoCNI, SagdLabCyclicHystSpatialParams));
NEW_TYPE_TAG(SagdLabCyclicHystBoxProblem, INHERITS_FROM(BoxModel, SagdLabCyclicHystProblem));
NEW_TYPE_TAG(SagdLabCyclicHystCCProblem, INHERITS_FROM(CCModel, SagdLabCyclicHystProblem));
    
// Set the grid type
SET_PROP(SagdLabCyclicHystProblem, Grid)
{
    typedef Dune::YaspGrid<2> type;
};

// Set the problem property
SET_PROP(SagdLabCyclicHystProblem, Problem)
{
    typedef Dumux::SagdLabCyclicHystProblem<TypeTag> type;
};

// Set the fluid system
SET_TYPE_PROP(SagdLabCyclicHystProblem,
              FluidSystem,
              Dumux::FluidSystems::H2OHeavyOil<TypeTag, typename GET_PROP_TYPE(TypeTag, Scalar)>);


// Enable gravity
SET_BOOL_PROP(SagdLabCyclicHystProblem, ProblemEnableGravity, true);

// Use forward differences instead of central differences
SET_INT_PROP(SagdLabCyclicHystProblem, ImplicitNumericDifferenceMethod, +1);

// Write newton convergence
SET_BOOL_PROP(SagdLabCyclicHystProblem, NewtonWriteConvergence, false);

SET_BOOL_PROP(SagdLabCyclicHystProblem, UseSimpleModel, true);

SET_BOOL_PROP(SagdLabCyclicHystProblem, UseMoles, true);

}


/*!
 * \ingroup ThreePTwoCNIBoxModel
 * \ingroup ImplicitTestProblems
 * \brief Non-isothermal problem where ...
 *
 * This problem uses the \ref ThreePTwoCNIModel.
 *
 *  */
template <class TypeTag >
class SagdLabCyclicHystProblem : public ImplicitPorousMediaProblem<TypeTag>
{
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GridView::Grid Grid;

    typedef ImplicitPorousMediaProblem<TypeTag> ParentType;

    // copy some indices for convenience
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
    enum {

        numPhases = GET_PROP_VALUE(TypeTag, NumPhases),
        numComponents = GET_PROP_VALUE(TypeTag, NumComponents),

        pressureIdx = Indices::pressureIdx,
        switch1Idx = Indices::switch1Idx,
        switch2Idx = Indices::switch2Idx,

        energyEqIdx = Indices::energyEqIdx,

        // phase and component indices
        wPhaseIdx = Indices::wPhaseIdx,
        nPhaseIdx = Indices::nPhaseIdx,
        gPhaseIdx = Indices::gPhaseIdx,
        wCompIdx = Indices::wCompIdx,
        nCompIdx = Indices::nCompIdx,

        // Phase State
        wPhaseOnly = Indices::wPhaseOnly,
        wnPhaseOnly = Indices::wnPhaseOnly,
        wgPhaseOnly = Indices::wgPhaseOnly,
        threePhases = Indices::threePhases,

        // Grid and world dimension
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld
    };


    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, VolumeVariables) VolumeVariables;

    typedef typename GET_PROP_TYPE(TypeTag, BoundaryTypes) BoundaryTypes;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GridView::template Codim<dim>::Entity Vertex;
    typedef typename GridView::Intersection Intersection;

    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
    typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;


    typedef typename GridView::template Codim<0>::Iterator ElementIterator;
    typedef typename GridView::template Codim<dim>::Iterator VertexIterator;

    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;

    enum { isBox = GET_PROP_VALUE(TypeTag, ImplicitIsBox) };
    enum { dofCodim = isBox ? dim : 0 };
        static const bool useMoles = GET_PROP_VALUE(TypeTag, UseMoles);
public:
    /*!
     * \brief The constructor
     *
     * \param timeManager The time manager
     * \param gridView The grid view
     */
    SagdLabCyclicHystProblem(TimeManager &timeManager, const GridView &gridView)
        : ParentType(timeManager, gridView), eps_(1e-6), pOut_(128768)  //128964.2 Pa = pressure at bottom of produdction well
    {
    name_ = GET_RUNTIME_PARAM(TypeTag, std::string, Problem.Name);        
    maxDepth_ = 4; // [m]
        FluidSystem::init();
    totalMassProducedOil_ =0;
    totalMassProducedWater_ =0;

        this->timeManager().startNextEpisode(11381); //episode lenght 11381 seconds

  
    //stateing in the console whether mole or mass fractions are used
        if(!useMoles)
        {
            std::cout<<"***** Problem uses mass-fractions. *****"<<std::endl;
        }
        else
        {
            std::cout<<"***** Problem uses mole-fractions. *****"<<std::endl;
        }
    }

      bool shouldWriteRestartFile() const
      {
          return
            //this->timeManager().timeStepIndex() ||
            this->timeManager().timeStepIndex() % 10000 == 0 ||
            //this->timeManager().episodeWillBeOver() ||
            this->timeManager().willBeFinished();
      }

      bool shouldWriteOutput() const
      {
          return
            this->timeManager().timeStepIndex() == 0 ||
            this->timeManager().timeStepIndex() % 10000 == 0 ||
            this->timeManager().episodeWillBeOver() ||
            this->timeManager().willBeFinished();
      }

    void postTimeStep()
    {
        double time = this->timeManager().time();
        double dt = this->timeManager().timeStepSize();
    
        // Calculate storage terms
        PrimaryVariables storage;
        this->model().globalStorage(storage);
        // Write mass balance information for rank 0
        if (this->gridView().comm().rank() == 0)
        {
            std::cout<<"Storage: " << storage << " Time: " << time+dt << std::endl;
            massBalance.open ("massBalanceCyclicHyst.txt", std::ios::out | std::ios::app );
                        massBalance << "         Storage       " << storage
                                    << "           Time           " << time+dt
                                    << std::endl; "\n";
                        massBalance.close();
        }
    

    this->spatialParams().getMaxSaturation(*this);
    this->spatialParams().trappedSat(*this);
    //this->spatialParams().addOutputVtkFields(*this);

    //mass of Oil
    Scalar newMassProducedOil_ = massProducedOil_;
    //std::cout<<" newMassProducedOil_ : "<< newMassProducedOil_ << " Time: " << time+dt << std::endl;

    totalMassProducedOil_ += newMassProducedOil_;
    //std::cout<<" totalMassProducedOil_ : "<< totalMassProducedOil_ << " Time: " << time+dt << std::endl; 
    //mass of Water
    Scalar newMassProducedWater_ = massProducedWater_;
    //std::cout<<" newMassProducedWater_ : "<< newMassProducedWater_ << " Time: " << time+dt << std::endl;

    totalMassProducedWater_ += newMassProducedWater_;
    //std::cout<<" totalMassProducedWater_ : "<< totalMassProducedWater_ << " Time: " << time+dt << std::endl;

    int timeStepIndex = this->timeManager().timeStepIndex();
    if (    this->timeManager().timeStepIndex() == 0 ||
            this->timeManager().timeStepIndex() % 10000 == 0 || //after every 1000000 secs
            this->timeManager().episodeWillBeOver() ||
            this->timeManager().willBeFinished()
    )

    {
    //std::cout<<" newMassProducedOil_ : "<< newMassProducedOil_ << " Time: " << time+dt << std::endl;
    std::cout<<" totalMassProducedOil_ : "<< totalMassProducedOil_ << " Time: " << time+dt << std::endl; 
    //std::cout<<" newMassProducedWater_ : "<< newMassProducedWater_ << " Time: " << time+dt << std::endl;
    std::cout<<" totalMassProducedWater_ : "<< totalMassProducedWater_ << " Time: " << time+dt << std::endl;
    }



    }

    void episodeEnd()
    {
        int indexEpisode = this->timeManager().episodeIndex();

        
    if (indexEpisode % 2 == 0){
           std::cout<<"Episode index is : "<<indexEpisode << ". Every 6th phase is an *Injection Phase*       "             <<std::endl;}

        else {
            std::cout<<"Episode index is : "<<indexEpisode << ". This is a NON-Injection Phase "
            <<std::endl;}

        this->timeManager().startNextEpisode(11381);   
            std::cout<<"Episode index is set to: "<<indexEpisode <<std::endl;
            //if (std::fmod(indexEpisode,2) == 0){
  
    }

    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const std::string name() const
    { return name_; }

    void sourceAtPos(PrimaryVariables &values,
                     const GlobalPosition &globalPos) const
    {
          values = 0.0;
    }

    // \}

    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment.
     *
     * \param values The boundary types for the conservation equations
     * \param globalPos The position for which the bc type should be evaluated
     */
   void boundaryTypesAtPos(BoundaryTypes &bcTypes,
            const GlobalPosition &globalPos) const
    {
        if (globalPos[1] <  eps_)                               // on bottom
        {
                bcTypes.setAllNeumann();
        }
        else if (globalPos[1] >  4.-eps_)                               // on top
        {
                bcTypes.setAllNeumann();                      
        }

        else if ( globalPos[0] > (2. - eps_) )
        {
                bcTypes.setAllDirichlet();
        }

        else                                                    // on Left 
             {
                bcTypes.setAllNeumann();
             }
    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        boundary segment.
     *
     * \param values The dirichlet values for the primary variables
     * \param globalPos The position for which the bc type should be evaluated
     *
     * For this method, the \a values parameter stores primary variables.
     */
    void dirichletAtPos(PrimaryVariables &values, const GlobalPosition &globalPos) const
    {

       initial_(values, globalPos);        // Everywhere else        

    }

    /*!
     * \brief Evaluate the boundary conditions for a neumann
     *        boundary segment.
     *
     * \param values The neumann values for the conservation equations
     * \param element The finite element
     * \param fvGeomtry The finite-volume geometry in the box scheme
     * \param is The intersection between element and boundary
     * \param scvIdx The local vertex index
     * \param boundaryFaceIdx The index of the boundary face
     *
     * For this method, the \a values parameter stores the mass flux
     * in normal direction of each phase. Negative values mean influx.
     */
    void solDependentNeumann(PrimaryVariables &values,
                      const Element &element,
                      const FVElementGeometry &fvGeometry,
                      const Intersection &is,
                      const int scvIdx,
                      const int boundaryFaceIdx,
                      const ElementVolumeVariables &elemVolVars) const
    {
        values = 0.0;
        int indexEpisode;
        indexEpisode = this->timeManager().episodeIndex();


        GlobalPosition globalPos;
        if (isBox)
           globalPos = element.geometry().corner(scvIdx);
        else
           globalPos = is.geometry().center();


        if (globalPos[1] > 1.64  &&                     // negative values for injection at injection well
            globalPos[1] < 1.72 )                      // 
        {
            if (indexEpisode % 2 == 0)                   //every 2nd episode is an injection phase -> 9
            {                              
                values[Indices::contiNEqIdx] = -0.0;  
                values[Indices::contiWEqIdx] = (-0.0143*2);  //(55.5 mol/kg*12.5 kg/h)/3600s = 0.193 Mol/s 
                                                                    //for model: 0.9375kg/h*55.5 /3600 = 0.014322917 Mol/s
                values[Indices::energyEqIdx] = (-641.*2); 
            }                                                        /*32875 kJ/hr /3600*1000 = 9132 J/s (field sized model)
                                                                   CALC for saturated steam
                                                                      specific enthalpy steam x=0.9@ 128964.2pa 
                                                                  = 2462 kJ/kg -> 44 kJ/mol
                                                                     44 kJ/mol*0.014 Mol/s = 641 J/s*/
           else

            {
                values[Indices::contiNEqIdx] = 0.0;
                values[Indices::contiWEqIdx] = 0.0;
                values[Indices::energyEqIdx] = 0.0;
            }

        }                                                                       
        

        else if (globalPos[1] > 1.2  && globalPos[1] < 1.28) // production well std values 0.18 to 0.26
        {

            //Scalar satWBound = 1.0;
            Scalar satW = elemVolVars[scvIdx].saturation(wPhaseIdx);                // Saturations
            Scalar satG = elemVolVars[scvIdx].saturation(gPhaseIdx);
            Scalar satN = elemVolVars[scvIdx].saturation(nPhaseIdx);
  
            Scalar elemPressW = elemVolVars[scvIdx].pressure(wPhaseIdx);            //Pressures
            Scalar elemPressG = elemVolVars[scvIdx].pressure(gPhaseIdx);
            Scalar elemPressN = elemVolVars[scvIdx].pressure(nPhaseIdx);
    
            Scalar densityW = elemVolVars[scvIdx].fluidState().density(wPhaseIdx);  //Densities
            Scalar densityG = elemVolVars[scvIdx].fluidState().density(gPhaseIdx);
            Scalar densityN = elemVolVars[scvIdx].fluidState().density(nPhaseIdx);
    
            Scalar moDensityW = elemVolVars[scvIdx].fluidState().molarDensity(wPhaseIdx);   //Molar Densities
            Scalar moDensityG = elemVolVars[scvIdx].fluidState().molarDensity(gPhaseIdx);
            Scalar moDensityN = elemVolVars[scvIdx].fluidState().molarDensity(nPhaseIdx);
    
            Scalar maDensityW = elemVolVars[scvIdx].fluidState().density(wPhaseIdx);   //Molar Densities
            Scalar maDensityG = elemVolVars[scvIdx].fluidState().density(gPhaseIdx);
            Scalar maDensityN = elemVolVars[scvIdx].fluidState().density(nPhaseIdx);
            Scalar elemMobW = elemVolVars[scvIdx].mobility(wPhaseIdx);      //Mobilities
            Scalar elemMobG = elemVolVars[scvIdx].mobility(gPhaseIdx);
            Scalar elemMobN = elemVolVars[scvIdx].mobility(nPhaseIdx);

            Scalar molFracWinW = elemVolVars[scvIdx].fluidState().moleFraction(wPhaseIdx, wCompIdx);
            Scalar molFracWinN = elemVolVars[scvIdx].fluidState().moleFraction(nPhaseIdx, wCompIdx);
            Scalar molFracWinG = elemVolVars[scvIdx].fluidState().moleFraction(gPhaseIdx, wCompIdx);
            Scalar molFracNinW = elemVolVars[scvIdx].fluidState().moleFraction(wPhaseIdx, nCompIdx);
            Scalar molFracNinN = elemVolVars[scvIdx].fluidState().moleFraction(nPhaseIdx, nCompIdx);
            Scalar molFracNinG = elemVolVars[scvIdx].fluidState().moleFraction(gPhaseIdx, nCompIdx);

            Scalar enthW = elemVolVars[scvIdx].enthalpy(wPhaseIdx);      //Mobilities
            Scalar enthG = elemVolVars[scvIdx].enthalpy(gPhaseIdx);
            Scalar enthN = elemVolVars[scvIdx].enthalpy(nPhaseIdx);

            Scalar wellRadius = 0.04; //0.50 * 0.3048; 0.50 ft as specified by SPE9
            Scalar wellArea = M_PI*std::pow(wellRadius,2);  // [m^2]


            Scalar gridHeight_ = 0.04;    
            //Scalar effectiveRadius_ = 0.208 * gridHeight_;    //Peaceman's Well Model. bin ja skeptisch...
            
            /*formulas based on Zhangxin Chen, 2008 Well models for various numerical models*/    
            Scalar effectiveRadius_ = sqrt((pow(gridHeight_,2)*3)/(3.1415*gridHeight_));  // = 0,218
         
            Scalar qW = (((2*3.1415*gridHeight_*4e-14)/(std::log(effectiveRadius_/wellRadius))) *    
            densityW * elemMobW * ( elemPressW-pOut_));

            //if (qW < 0) qW = 0; check valve
    
            Scalar qN = (((2*3.1415*gridHeight_*4e-14)/(std::log(effectiveRadius_/wellRadius))) * 
            densityN * elemMobN  * (elemPressN-pOut_));

            //if (qN < 0) qN = 0; check valve

            //without cooling:
            //Scalar qE = qW*0.018*enthW + qN*enthN*0.350;  

            //if (qW < 0) qW = 0; //check valve

            //Cooling to ensure liquid pool at production well
               Scalar wT = elemVolVars[scvIdx].temperature(); // well temperature
                Scalar qE;
                
                if ( wT > 373. ) 
                {
                    qE = qW*0.018*enthW + qN*enthN*0.350 + (wT - 373.) * 500.;
                }
                else
                {
                    qE = qW*0.018*enthW + qN*enthN*0.350;
                }
        //end of cooling mechanism
  
       values[Indices::contiWEqIdx] = qW;

           values[Indices::contiNEqIdx] = qN;
          
           values[Indices::energyEqIdx] = qE;

       massProducedOil_ = qN;
       massProducedWater_ = qW;

        }
}

    // \}

    /*!
     * \name Volume terms
     */
    // \{

    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * \param values The initial values for the primary variables
     * \param globalPos The position for which the initial condition should be evaluated
     *
     * For this method, the \a values parameter stores primary
     * variables.
     */
    void initialAtPos(PrimaryVariables &values, const GlobalPosition &globalPos) const
    {
        initial_(values, globalPos);
    }

    /*!
     * \brief Return the initial phase state inside a control volume.
     *
     * \param vert The vertex
     * \param globalIdx The index of the global vertex
     * \param globalPos The global position
     */
    int initialPhasePresence(const Vertex &vert,
                             int &globalIdx,
                             const GlobalPosition &globalPos) const
    {
        return wnPhaseOnly;
        //return threePhases;
    }

    void addOutputVtkFields()
                {
                    typedef Dune::BlockVector<Dune::FieldVector<double, 1> > ScalarField;
                    unsigned numVertices = this->gridView().size(dim);

                    //create required scalar fields
                    ScalarField *trapSatN = this->resultWriter().allocateManagedBuffer(numVertices);

                    //Fill the scalar fields with values
                    unsigned numElements = this->gridView().size(0);
                    ScalarField *rank = this->resultWriter().allocateManagedBuffer(numElements);

                    FVElementGeometry fvElemGeom;
                    VolumeVariables volVars;


                    ElementIterator elemIt = this->gridView().template begin<0>();
                    ElementIterator elemEndIt = this->gridView().template end<0>();
                    for (; elemIt != elemEndIt; ++elemIt)
                    {
                        int idx = this->elementMapper().map(*elemIt);
                        (*rank)[idx] = this->gridView().comm().rank();
                        fvElemGeom.update(this->gridView(), *elemIt);

                        int numVerts = elemIt->template count<dim> ();

                        for (int i = 0; i < numVerts; ++i)
                        {
                            int globalIdx = this->vertexMapper().map(*elemIt, i, dim);
                            

                if (this->timeManager().timeStepIndex() == 0){
                  (*trapSatN)[globalIdx] = 0;}
                else {                           
                (*trapSatN)[globalIdx] = this->spatialParams().trapSats_[globalIdx].trappedSatN; 
                                }           
                }
                    }

                    this->resultWriter().attachVertexData(*trapSatN, "trapSatN");

                }
 
private:
    // internal method for the initial condition (reused for the
    // dirichlet conditions!)
    void initial_(PrimaryVariables &values,
                  const GlobalPosition &globalPos) const
    {
        Scalar densityW = 1000.0;
        values[pressureIdx] = 101300. + (maxDepth_ - globalPos[1])*densityW*9.81;  //= 128,964.2 at bottom of prod well
                                                                                  

        values[switch1Idx] = 295.13;   // temperature
        values[switch2Idx] = 0.3;        //NAPL saturation
    }

    Scalar maxDepth_;
    Scalar eps_;
    Scalar pIn_;
    Scalar pOut_;
    Scalar episodeLength_;
    Scalar indexEpisode;
    std::string name_;
    Scalar totalMassProducedOil_;
    Scalar totalMassProducedWater_;
    std::ofstream massBalance;

};
} //end namespace

#endif
