
#install headers
install(FILES
parkerVanGenZero3p.hh
parkerVanGenZero3pparams.hh
parkerVanGenZeroHyst3p.hh
parkerVanGenZeroHyst3pparams.hh
DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/dumux/material/fluidmatrixinteractions/3p)
