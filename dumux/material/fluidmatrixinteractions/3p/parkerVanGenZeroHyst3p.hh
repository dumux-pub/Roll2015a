// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file 
 * 
 * \brief Implementation of van Genuchten's capillary pressure-saturation relation.
 * 
 */
#ifndef PARKERVANGENZEROHYST_3P_HH
#define PARKERVANGENZEROHYST_3P_HH

#include "parkerVanGenZeroHyst3pparams.hh"

#include <algorithm>


namespace Dumux
{
/*!
 * \ingroup material
 *
 * \brief Implementation of van Genuchten's capillary pressure <->
 *        saturation relation. This class bundles the "raw" curves
 *        as static members and doesn't concern itself converting
 *        absolute to effective saturations and vince versa.
 *
 * \sa VanGenuchten, VanGenuchtenThreephase
 */
template <class ScalarT, class ParamsT = ParkerVanGenZeroHyst3PParams<ScalarT> >
class ParkerVanGenZeroHyst3P
{

public:
    typedef ParamsT Params;
    typedef typename Params::Scalar Scalar;

    /*!
     * \brief The capillary pressure-saturation curve.
     *
     */
    static Scalar pc(const Params &params, Scalar sw)
    {
        DUNE_THROW(Dune::NotImplemented, "Capillary pressures for three phases is not so simple! Use pcgn, pcnw, and pcgw");
    }

    DUNE_DEPRECATED_MSG("use pc() (uncapitalized 'c') instead")
    static Scalar pC(const Params &params, Scalar swe)
    {
        return pc(params, swe);
    }

    static Scalar pcgw(const Params &params, Scalar sw)
    {
        return 0;
    }

    DUNE_DEPRECATED_MSG("use pcgw() (uncapitalized 'cgw') instead")
    static Scalar pCGW(const Params &params, Scalar sw)
    {
        return pcgw(params, sw);
    }

    static Scalar pcnw(const Params &params, Scalar sw)
    {
        return 0;
    }

    DUNE_DEPRECATED_MSG("use pcnw() (uncapitalized 'cnw') instead")
    static Scalar pCNW(const Params &params, Scalar sw)
    {
        return pcnw(params, sw);
    }

    static Scalar pcgn(const Params &params, Scalar St)
    {
        return 0;
    }

    DUNE_DEPRECATED_MSG("use pcgn() (uncapitalized 'cgn') instead")
    static Scalar pCGN(const Params &params, Scalar St)
    {
        return pcgn(params, St);
    }

    static Scalar pcAlpha(const Params &params, Scalar sn)
    {
        return(1);
    }

    DUNE_DEPRECATED_MSG("use pcAlpha() (uncapitalized 'c') instead")
    static Scalar pCAlpha(const Params &params, Scalar sn)
    {
        return pcAlpha(params, sn);
    }

    /*!
     * \brief The saturation-capillary pressure curve.
     *
     */
    static Scalar sw(const Params &params, Scalar pc)
    {
        DUNE_THROW(Dune::NotImplemented, "sw(pc) for three phases not implemented! Do it yourself!");
    }


    DUNE_DEPRECATED_MSG("use sw() (uncapitalized 's') instead")
    static Scalar Sw(const Params &params, Scalar pc)
    {
        return sw(params, pc);
    }

    /*!
     * \brief Returns the partial derivative of the capillary
     *        pressure to the effective saturation.
     *
    */
    static Scalar dpc_dsw(const Params &params, Scalar sw)
    {
        DUNE_THROW(Dune::NotImplemented, "dpc/dsw for three phases not implemented! Do it yourself!");
    }

    DUNE_DEPRECATED_MSG("use dpc_dsw() (uncapitalized 'c', 's') instead")
    static Scalar dpC_dSw(const Params &params, Scalar sw)
    {
        return dpc_dsw(params, sw);
    }

    /*!
     * \brief Returns the partial derivative of the effective
     *        saturation to the capillary pressure.
     */
    static Scalar dsw_dpc(const Params &params, Scalar pc)
    {
        DUNE_THROW(Dune::NotImplemented, "dsw/dpc for three phases not implemented! Do it yourself!");
    }

    DUNE_DEPRECATED_MSG("use dsw_dpc() (uncapitalized 's', 'c') instead")
    static Scalar dSw_dpC(const Params &params, Scalar pc)
    {
        return dsw_dpc(params, pc);
    }

    /*!
     * \brief The relative permeability for the wetting phase of
     *        the medium implied by van Genuchten's
     *        parameterization.
     *
     * The permeability of water in a 3p system equals the standard 2p description.
     * (see p61. in "Comparison of the Three-Phase Oil Relative Permeability Models"
     * MOJDEH  DELSHAD and GARY A. POPE, Transport in Porous Media 4 (1989), 59-83.)
     *
     * \param sn saturation of the NAPL phase.
     * \param sg saturation of the gas phase.
     * \param saturation saturation of the water phase.
     * \param params Array of parameters.
     */
    static Scalar krw(const Params &params,  Scalar saturation, Scalar sn, Scalar sg)
    {

        //transformation to effective saturation
        //Scalar se = (saturation - params.swr()) / (1-params.swr());
    Scalar se = (saturation - params.swr()) / (1-params.TrappedSatN()-params.swr());



        /* regularization */
        if(se > 1.0) return 1.;
        if(se < 0.0) return 0.;

        Scalar r = 1. - std::pow(1 - std::pow(se, 1/params.vgm()), params.vgm());
        return std::sqrt(se)*r*r;
    };

    /*!
     * \brief The relative permeability for the non-wetting phase
     *        after the Model of Parker et al. (1987).
     *
     * See model 7 in "Comparison of the Three-Phase Oil Relative Permeability Models"
     * MOJDEH  DELSHAD and GARY A. POPE, Transport in Porous Media 4 (1989), 59-83.
     * or more comprehensive in
     * "Estimation of primary drainage three-phase relative permeability for organic
     * liquid transport in the vadose zone", Leonardo I. Oliveira, Avery H. Demond,
     * Journal of Contaminant Hydrology 66 (2003), 261-285
     *
     *
     * \param sw saturation of the water phase.
     * \param sg saturation of the gas phase.
     * \param saturation saturation of the NAPL phase.
     * \param params Array of parameters.
     */
    static Scalar krn(const Params &params, Scalar sw, Scalar saturation, Scalar sg)
    {

        Scalar swe = std::min((sw - params.swr()) / (1 - params.TrappedSatN()- params.swr()), 1.);
        Scalar ste = std::min((sw +  saturation - params.swr()) / (1 - params.swr()), 1.);

        // regularization
        if(swe <= 0.0) swe = 0.;
        if(ste <= 0.0) ste = 0.;
        if(ste - swe <= 0.0) return 0.;

        Scalar krn_;
        krn_ = std::pow(1 - std::pow(swe, 1/params.vgm()), params.vgm());
        krn_ -= std::pow(1 - std::pow(ste, 1/params.vgm()), params.vgm());
        krn_ *= krn_;

        if (params.krRegardsSnr())
        {
            // regard Snr in the permeability of the n-phase, see Helmig1997
            Scalar resIncluded = std::max(std::min((saturation - params.snr()/ (1-params.swr())), 1.), 0.);
            krn_ *= std::sqrt(resIncluded );
        }
        else
            krn_ *= std::sqrt(saturation / (1 - params.swr()));   // Hint: (ste - swe) = sn / (1-Srw)


        return krn_;
    };


    /*!
     * \brief The relative permeability for the non-wetting phase
     *        of the medium implied by van Genuchten's
     *        parameterization.
     *
     * The permeability of gas in a 3p system equals the standard 2p description.
     * (see p61. in "Comparison of the Three-Phase Oil Relative Permeability Models"
     * MOJDEH  DELSHAD and GARY A. POPE, Transport in Porous Media 4 (1989), 59-83.)
     *
     * \param sw saturation of the water phase.
     * \param sn saturation of the NAPL phase.
     * \param saturation saturation of the gas phase.
     * \param params Array of parameters.
     */
    static Scalar krg(const Params &params, Scalar sw, Scalar sn, Scalar saturation)
    {

        // se = (sw+sn - Sgr)/(1-Sgr)
        Scalar se = std::min(((1-saturation) - params.sgr()) / (1 - params.sgr()), 1.);


        /* regularization */
        if(se > 1.0) return 0.0;
        if(se < 0.0) return 1.0;
        Scalar scalFact = 1.;
        if (saturation<=0.1)
        {
          scalFact = (saturation - params.sgr())/(0.1 - params.sgr());
          if (scalFact < 0.) scalFact = 0.;
        }

        Scalar result = scalFact * std::pow(1 - se, 1.0/3.) * std::pow(1 - std::pow(se, 1/params.vgm()), 2*params.vgm());

        return result;
    };

    /*!
     * \brief The relative permeability for a phase.
     * \param sw saturation of the water phase.
     * \param sg saturation of the gas phase.
     * \param sn saturation of the NAPL phase.
     * \param params Array of parameters.
     * \param phase indicator, The saturation of all phases.
     */
    static Scalar kr(const Params &params, const int phase, const Scalar sw, const Scalar sn, const Scalar sg)

    {
        switch (phase)
        {
        case 0:
            return krw(params, sw, sn, sg);
            break;
        case 1:
            return krn(params, sw, sn, sg);
            break;
        case 2:
            return krg(params, sw, sn, sg);
            break;
        }
        return 0;
    };

   /*
    * \brief the basis for calculating adsorbed NAPL in storage term
    * \param bulk density of porous medium, adsorption coefficient
    */
   static Scalar bulkDensTimesAdsorpCoeff (const Params &params)
   {
      return params.rhoBulk() * params.KdNAPL();
   }
};
}

#endif
