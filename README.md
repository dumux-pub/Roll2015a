Readme
===============================

Content
--------------

The content of this DUNE module was extracted from the module /temp/stefanr/dumux/dumux-devel.
In particular, the following subfolders of /temp/stefanr/dumux/dumux-devel have been extracted:

- appl/sagd
- appl/sagdLab
- appl/sagdCyclic
- appl/sagdLabCyclic
- appl/sagdCyclicHyst
- appl/sagdLabCyclicHyst

Additionally, all headers in /temp/stefanr/dumux/dumux-devel that are required to build the
executables from the sources

- appl/sagd/sagd.cc
- appl/sagdLab/sagdLab.cc
- appl/sagdCyclic/sagdCyclic.cc
- appl/sagdLabCyclic/sagdLabCyclic.cc
- appl/sagdCyclicHyst/sagdCyclicHyst.cc
- appl/sagdLabCyclicHyst/sagdLabCyclicHyst.cc

have been extracted. You can build the module just like any other DUNE
module. For building and running the executables, please go to the folders
containing the sources listed above.

Installation
============

You can build the module just like any other DUNE
module. For building and running the executables, please go to the folders
containing the sources listed above. For the basic
dependencies see dune-project.org.

The easiest way is to use the `installRoll2015a.sh` in this folder.
You might want to look at it before execution [here](https://git.iws.uni-stuttgart.de/dumux-pub/Roll2015a/raw/master/installRoll2015a.sh). Create a new folder containing the script and execute it.
You can copy the following to a terminal:
```bash
mkdir -p Roll2015a && cdRoll2015a
wget -q https://git.iws.uni-stuttgart.de/dumux-pub/Roll2015a/raw/master/installRoll2015a.sh
chmod +x installRoll2015a.sh && ./installRoll2015a.sh
```
It will install this module will take care of most dependencies.
For the basic dependencies see dune-project.org and at the end of this README.

Application
---------------
Once built please find the executables in the folder
`Roll2015a/build-cmake/:

- appl/sagd/sagd.cc
- appl/sagdLab/sagdLab.cc
- appl/sagdCyclic/sagdCyclic.cc
- appl/sagdLabCyclic/sagdLabCyclic.cc
- appl/sagdCyclicHyst/sagdCyclicHyst.cc
- appl/sagdLabCyclicHyst/sagdLabCyclicHyst.cc

Used Version
--------------------

When this module was created, the original module /temp/stefanr/dumux/dumux-devel was using
the following list of DUNE modules and third-party libraries.
BEWARE: This does not necessarily mean that the applications in this
extracted module actually depend on all these components.

- dune-common 2.4
- dune-geometry 2.4
- dune-grid 2.4
- dune-istl 2.4
- dune-localfuntions 2.4

- dumux-stable 2.8

Use the script installRoll2015a.sh to install these modules and execute dunecontrol.

## Installation with Docker

Create a new folder and change into it

```bash
mkdir Roll2015a
cd Roll2015a
```

Then download the container startup script
```bash
wget https://git.iws.uni-stuttgart.de/dumux-pub/Roll2015a/-/raw/master/docker_roll2015a.sh
```

and open the docker container by running
```bash
bash docker_roll2015a.sh open
```

After the script has run successfully, executables can be built and run with an input file, e.g., 
```bash
cd Roll2015a/build-cmake/appl/sagd
make sagd
./sagd sagd.input
```
